//
// Created by Артур Кулапин on 22.02.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR4_HEURISTICS_MISMATCHES_HEURISTIC_HPP_
#define TEACHING_SEMESTER_2_SEMINAR4_HEURISTICS_MISMATCHES_HEURISTIC_HPP_

#include "abstract_heuristic.hpp"

class MismatchesHeuristic : public AbstractHeuristic {
 public:
  double operator()(const State& state) {
    return 1.0;
  }
};

#endif //TEACHING_SEMESTER_2_SEMINAR4_HEURISTICS_MISMATCHES_HEURISTIC_HPP_
