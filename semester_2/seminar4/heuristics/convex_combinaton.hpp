//
// Created by Артур Кулапин on 22.02.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR4_HEURISTICS_CONVEX_COMBINATON_HPP_
#define TEACHING_SEMESTER_2_SEMINAR4_HEURISTICS_CONVEX_COMBINATON_HPP_

#include "empty_manhattan.hpp"
#include "mismatches_heuristic.hpp"

#include <vector>

class ConvexCombination : public AbstractHeuristic {
 public:
  ConvexCombination(const std::vector<AbstractHeuristic*>& heuristics,
                    const std::vector<double>& lambdas) :
                    heuristics_(heuristics), lambdas_(lambdas) {}

  double operator()(const State& state) {
    double res = 0.;
    for (int i = 0; i < heuristics_.size(); ++i) {
      res += heuristics_[i]->operator()(state) * lambdas_[i];
    }
    return res;
  }

 private:
  std::vector<AbstractHeuristic*> heuristics_;
  std::vector<double> lambdas_;
};

#endif //TEACHING_SEMESTER_2_SEMINAR4_HEURISTICS_CONVEX_COMBINATON_HPP_
