//
// Created by Артур Кулапин on 22.02.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR4_HEURISTICS_ABSTRACT_HEURISTIC_HPP_
#define TEACHING_SEMESTER_2_SEMINAR4_HEURISTICS_ABSTRACT_HEURISTIC_HPP_

struct State {};

class AbstractHeuristic {
 public:
  virtual double operator()(const State&) = 0;
};


#endif //TEACHING_SEMESTER_2_SEMINAR4_HEURISTICS_ABSTRACT_HEURISTIC_HPP_
