//
// Created by Артур Кулапин on 22.02.2023.
//

#include "heuristics/convex_combinaton.hpp"

#include <iostream>

int main() {
  auto* m_h = new MismatchesHeuristic();
  auto* e_h = new EmptyManhattan();
  ConvexCombination comb({m_h, e_h}, {0.1, 0.2});
  std::cout << comb(State());
  delete m_h;
  delete e_h;
}
