//
// Created by Артур Кулапин on 22.02.2023.
//

template <typename Vertex, class Graph, class Heuristic, class Visitor>
class Astar {
 public:
  void Find(const Vertex& s, const Graph& graph,
            Heuristic heuristic, Visitor& visitor);
 private:
};

