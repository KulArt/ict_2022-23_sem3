//
// Created by Артур Кулапин on 05.04.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR10_ABSTRACT_FLOW_SEARCHER_HPP_
#define TEACHING_SEMESTER_2_SEMINAR10_ABSTRACT_FLOW_SEARCHER_HPP_

#include "../networks/network.hpp"


template <class FlowNetwork>
class AbstractFlowSearcher {
 public:
  explicit AbstractFlowSearcher(FlowNetwork network) : network_(std::move(network)) {}

  virtual int FindMaxFlow() = 0;

 protected:
  FlowNetwork network_;
};

#endif //TEACHING_SEMESTER_2_SEMINAR10_ABSTRACT_FLOW_SEARCHER_HPP_
