//
// Created by Артур Кулапин on 05.04.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR10_EDMONDS_KARP_HPP_
#define TEACHING_SEMESTER_2_SEMINAR10_EDMONDS_KARP_HPP_

#include "abstract_flow_searcher.hpp"
#include "../graphs/traverses/bfs/visitors/ancestor_calculator.hpp"
#include "../graphs/traverses/bfs/bfs.hpp"
#include "../utils.hpp"

template <class FlowNetwork>
class EdmondsKarpAlgorithm : public AbstractFlowSearcher<FlowNetwork> {
 public:
  using BaseClass = AbstractFlowSearcher<FlowNetwork>;

  explicit EdmondsKarpAlgorithm(FlowNetwork network) : BaseClass(std::move(network)) {}

  int FindMaxFlow() {
    int max_flow = 0;
    for (auto ancestors = AncestorsInShortestPathsTree(); ancestors[BaseClass::network_.Sink()];
         ancestors = AncestorsInShortestPathsTree()) {
      auto edges_id_on_path = PathEdges(ancestors);
      int extra_flow = ExtraFlow(edges_id_on_path);
      for (const auto& edge : edges_id_on_path) {
        BaseClass::network_.UpdateFlowByEdge(edge, extra_flow);
      }
      max_flow += extra_flow;
    }
    return max_flow;
  }

 private:
  using Edge = typename FlowNetwork::Edge;
  using OptionalEdgeVector = std::vector<std::optional<Edge>>;

  OptionalEdgeVector AncestorsInShortestPathsTree() {
    OptionalEdgeVector ancestors(BaseClass::network_.NumberVertices());
    graphs::traverses::visitors::AncestorsCalculator<typename FlowNetwork::Vertex, typename FlowNetwork::Edge> ancestors_calculator(&ancestors);
    auto residual_network = BaseClass::network_.ResidualNetworkView();
    graphs::traverses::BreadthFirstSearch(BaseClass::network_.Source(), residual_network, ancestors_calculator);
    return ancestors;
  }

  std::vector<Edge> PathEdges(const OptionalEdgeVector& ancestors) {
    std::vector<Edge> edges_id;
    int cur_vertex = BaseClass::network_.Sink();
    while (cur_vertex != BaseClass::network_.Source()) {
      edges_id.push_back(ancestors[cur_vertex].value());
      cur_vertex = ancestors[cur_vertex]->GetFrom();
    }
    std::reverse(edges_id.begin(), edges_id.end());
    return edges_id;
  }

  int ExtraFlow(const std::vector<Edge>& edges_id_on_path) {
    int min_capacity = constants::kInf;
    for (const auto& edge : edges_id_on_path) {
      min_capacity = std::min(min_capacity, BaseClass::network_.ResidualCapacity(edge));
    }
    return min_capacity;
  }
};

#endif //TEACHING_SEMESTER_2_SEMINAR10_EDMONDS_KARP_HPP_
