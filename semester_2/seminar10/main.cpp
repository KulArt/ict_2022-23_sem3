//
// Created by Артур Кулапин on 05.04.2023.
//

#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

#include "flow_searchers/edmonds_karp.hpp"
#include "networks/gold_network_builder.hpp"
#include "utils.hpp"

/**
 * \brief Функция поиска минимального значения из промежутка [from, to), для которого верен бинарный предикат, где один из аргументов фиксирован
 * \tparam UnaryPredicate - тип унарного предиката, который на основе числа возвращает bool
 * \param from - начало диапазона, в котором осуществляется поиск
 * \param to - конец диапазона, в котором осуществляется поиск (на один больше последнего элемента)
 * \param predicate - сам предикат
 * \returns Искомый элемент диапазона при его наличии или to при отсутствии такового
 * \par Сложность: число вызовов предиката составляет log_2(to - from) + O(1)
 */
template<class UnaryPredicate>
int BinarySearch(int from, int to, UnaryPredicate predicate) {
  int mid = from;
  while (to > from) {
    mid = from + (to - from) / 2;
    if (predicate(mid)) {
      from = ++mid;
    } else {
      to = mid;
    }
  }
  return mid;
}

template<typename T>
std::vector<T> ReadVector(int number_elements) {
  std::vector<T> result(number_elements);
  for (auto& elem : result) {
    std::cin >> elem;
  }
  return result;
}

std::vector<std::pair<int, int>> ReadGraph(int number_edges, std::istream& is = std::cin) {
  std::vector<std::pair<int, int>> edges;
  int from, to;
  for (int i = 0; i < number_edges; ++i) {
    is >> from >> to;
    edges.emplace_back(--from, --to);
  }
  return edges;
}

template <typename Vertex = int,
          typename Edge = DefaultEdge<Vertex>,
          class Graph = graphs::Graph<Vertex, Edge>,
          class FlowNetwork = FlowNetwork<Graph>>
FlowNetwork BuildConfidenceNetwork(const std::vector<std::pair<int, int>>& edges,
                                   const std::vector<int>& gold_amount,
                                   int number_vertices, int gold_limit) {

  GoldNetworkBuilder<FlowNetwork> builder(number_vertices + 2);
  builder.AssignSource(number_vertices);
  builder.AssignSink(number_vertices + 1);
  for (int cur_vertex = 0; cur_vertex < number_vertices; ++cur_vertex) {
    builder.AddEdge(Edge{number_vertices, cur_vertex}, gold_amount[cur_vertex]);
    builder.AddEdge({cur_vertex, number_vertices + 1}, gold_limit);
  }
  for (const auto& [from, to] : edges) {
    builder.AddEdge({from, to}, constants::kInf);
  }
  return builder.GetFlowNetwork();
}

int main() {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int people_number, confidence_pairs;
  std::cin >> people_number >> confidence_pairs;
  auto gold_amount = ReadVector<int>(people_number);
  auto edges = ReadGraph(confidence_pairs);

  int total_gold = std::accumulate(gold_amount.begin(), gold_amount.end(), 0);
  int max_gold = std::accumulate(gold_amount.begin(), gold_amount.end(),
                                 0, [](int value, int elem) {
        return std::max(value, elem);
      });
  auto binary_predicate = [&edges, &gold_amount, total_gold, people_number](int gold_limit) {
    auto network = BuildConfidenceNetwork<>(edges, gold_amount, people_number, gold_limit);
    EdmondsKarpAlgorithm flow_searcher(network);
    return flow_searcher.FindMaxFlow() < total_gold;
  };
  std::cout << BinarySearch(total_gold / people_number, max_gold + 1, binary_predicate) << '\n';
  return 0;
}