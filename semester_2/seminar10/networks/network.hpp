//
// Created by Артур Кулапин on 05.04.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR10_NETWORK_HPP_
#define TEACHING_SEMESTER_2_SEMINAR10_NETWORK_HPP_

#include "../graphs/filtered_graph.hpp"

using namespace graphs;
// TODO (students): add Nnetwork and network builder namespaces.

template<typename FlowNetwork>
class GoldNetworkBuilder;

template<typename GraphType>
class FlowNetwork {
 public:
  using Vertex = typename GraphType::Vertex;
  using Edge = typename GraphType::Edge;
  using Graph = GraphType;

  explicit FlowNetwork(Graph graph) : graph_(std::move(graph)) {}

  void UpdateFlowByEdge(const Edge &edge, int extra_flow) {
    edges_properties_[edge].flow += extra_flow;
    const auto reverse_edge = GetReverseEdge<Vertex>(edge);
    edges_properties_[reverse_edge].flow -= extra_flow;
  }
  // TODO (students): Fix const
  int ResidualCapacity(const Edge &edge) {
    return edges_properties_[edge].capacity - edges_properties_[edge].flow;
  }

  size_t NumberEdges() const {
    return graph_.EdgesCount();
  }

  size_t NumberVertices() const {
    return graph_.VerticesCount();
  }

  Vertex& Source() {
    return source_;
  }

  Vertex& Sink() {
    return sink_;
  }

  graphs::FilteredGraph<GraphType> ResidualNetworkView() {
    return FilteredGraph(graph_, [this](const Edge &edge) {
      return ResidualCapacity(edge) > 0;
    });
  }

  friend class GoldNetworkBuilder<FlowNetwork>;

 private:
  struct EdgeProperties {
    EdgeProperties() = default;
    EdgeProperties(int flow, int capacity,
                   size_t reverse_edge_id) : flow(flow), capacity(capacity),
                                             reverse_edge_id(reverse_edge_id) {}
    int flow = 0;
    int capacity = 0;
    size_t reverse_edge_id = 0;
  };

  Vertex source_ = 0;
  Vertex sink_ = 0;
  std::unordered_map<Edge, EdgeProperties> edges_properties_;
  GraphType graph_;
};

#endif //TEACHING_SEMESTER_2_SEMINAR10_NETWORK_HPP_
