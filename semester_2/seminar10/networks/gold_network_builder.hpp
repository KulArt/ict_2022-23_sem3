//
// Created by Артур Кулапин on 05.04.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR10_NETWORK_BUILDER_HPP_
#define TEACHING_SEMESTER_2_SEMINAR10_NETWORK_BUILDER_HPP_

#include <utility>

#include "network.hpp"

template<class FlowNetwork>
class GoldNetworkBuilder {
 public:
  using Vertex = typename FlowNetwork::Vertex;
  using Edge = typename FlowNetwork::Edge;

  explicit GoldNetworkBuilder(size_t vertices_number) : network_(Graph(vertices_number)) {}

  void AddEdge(const Edge &edge, int capacity) {
    network_.edges_properties_[edge] = {
        0, capacity, network_.edges_properties_.size() + 1
    };
    network_.graph_.AddEdge(edge);

    const auto reverse_edge = GetReverseEdge<Vertex>(edge);
    network_.edges_properties_[reverse_edge] = {
        0, 0, network_.edges_properties_.size() - 1
    };
    network_.graph_.AddEdge(reverse_edge);
  }

  void AssignSource(const Vertex& source) { network_.Source() = source; }

  void AssignSink(const Vertex& sink) { network_.Sink() = sink; }

  FlowNetwork GetFlowNetwork() const {
    return std::move(network_);
  }

 private:
  FlowNetwork network_;
};

#endif //TEACHING_SEMESTER_2_SEMINAR10_NETWORK_BUILDER_HPP_
