//
// Created by Артур Кулапин on 05.04.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR10_ABSTRACT_BFS_VISITOR_HPP_
#define TEACHING_SEMESTER_2_SEMINAR10_ABSTRACT_BFS_VISITOR_HPP_

namespace graphs::traverses::visitors {
template<class Vertex, class Edge>
class AbstractBfsVisitor {
 public:
  virtual void TreeEdge(const Edge & /*edge*/) {}
  virtual ~AbstractBfsVisitor() = default;
};
}

#endif //TEACHING_SEMESTER_2_SEMINAR10_ABSTRACT_BFS_VISITOR_HPP_
