//
// Created by Артур Кулапин on 05.04.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR10_ANCESTOR_CALCULATOR_HPP_
#define TEACHING_SEMESTER_2_SEMINAR10_ANCESTOR_CALCULATOR_HPP_

#include <optional>
#include <vector>

#include "abstract_bfs_visitor.hpp"

namespace graphs::traverses::visitors {
template<class Vertex, class Edge>
class AncestorsCalculator : public AbstractBfsVisitor<Vertex, Edge> {
 public:
  using OptionalEdgeVector = std::vector<std::optional<Edge>>;

  explicit AncestorsCalculator(OptionalEdgeVector *edges_to_ancestor_in_bfs_tree)
      :
      edges_to_ancestor_in_bfs_tree_(edges_to_ancestor_in_bfs_tree) {}

  void TreeEdge(const Edge &edge) override {
    (*edges_to_ancestor_in_bfs_tree_)[edge.GetTo()] = edge;
  }

 private:
  OptionalEdgeVector *edges_to_ancestor_in_bfs_tree_;
};
}

#endif //TEACHING_SEMESTER_2_SEMINAR10_ANCESTOR_CALCULATOR_HPP_
