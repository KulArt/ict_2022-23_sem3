//
// Created by Артур Кулапин on 05.04.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR10_BFS_HPP_
#define TEACHING_SEMESTER_2_SEMINAR10_BFS_HPP_

#include <queue>
#include <unordered_set>

namespace graphs::traverses {
template<class Vertex, class Graph, class Visitor>
// TODO (students): Make graph constant.
void BreadthFirstSearch(Vertex origin_vertex, Graph &graph, Visitor &visitor) {
  std::queue<Vertex> bfs_queue;
  std::unordered_set<Vertex> visited_vertices;

  bfs_queue.push(origin_vertex);
  visited_vertices.insert(origin_vertex);

  while (!bfs_queue.empty()) {
    auto cur_vertex = bfs_queue.front();
    bfs_queue.pop();
    for (auto &edge : graph.EdgesFromVertex(cur_vertex)) {
      auto neighbour = edge.GetTo();
      if (visited_vertices.find(neighbour) == visited_vertices.end()) {
        visitor.TreeEdge(edge);
        bfs_queue.push(neighbour);
        visited_vertices.insert(neighbour);
      }
    }
  }
}
}

#endif //TEACHING_SEMESTER_2_SEMINAR10_BFS_HPP_
