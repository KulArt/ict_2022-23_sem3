//
// Created by Артур Кулапин on 05.04.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR10_FILTERED_GRAPH_HPP_
#define TEACHING_SEMESTER_2_SEMINAR10_FILTERED_GRAPH_HPP_

#include "graph.hpp"

namespace graphs {
template<class Graph>
class FilteredGraph {
 public:
  // TODO (students): Fix const graph.
  FilteredGraph(Graph &graph,
                std::function<bool(typename Graph::Edge)> predicate) :
      graph_(graph), predicate_(std::move(predicate)) {}

  // Implement FilterIterator and IteratorRange!!
  // TODO (students): Fix const.
  typename Graph::InternalEdgeContainer EdgesFromVertex(typename Graph::Vertex &vertex) {
    auto outgoing_edges = graph_.EdgesFromVertex(vertex);
    typename Graph::InternalEdgeContainer filtered_edges;
    for (const auto &edge : outgoing_edges) {
      if (predicate_(edge)) {
        filtered_edges.insert(edge);
      }
    }
    return filtered_edges;
  }

  size_t VerticesCount() const {
    return graph_.VerticesCount();
  }

 private:
  // TODO (students): Fix const.
  Graph &graph_;
  std::function<bool(typename Graph::Edge)> predicate_;
};
}

#endif //TEACHING_SEMESTER_2_SEMINAR10_FILTERED_GRAPH_HPP_
