//
// Created by Артур Кулапин on 05.04.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR10_GRAPH_HPP_
#define TEACHING_SEMESTER_2_SEMINAR10_GRAPH_HPP_

#include <unordered_map>
#include <unordered_set>

template<typename Vertex>
struct DefaultEdge : std::pair<Vertex, Vertex> {
  using BaseClass = std::pair<Vertex, Vertex>;

  DefaultEdge(const Vertex &first, const Vertex &second) :
      BaseClass(first, second) {}

  const Vertex &GetFrom() const { return BaseClass::first; }
  const Vertex &GetTo() const { return BaseClass::second; }
};

// Pay attention! It is custom hash for template structure!
namespace std {
template<typename Vertex>
struct hash<DefaultEdge < Vertex>> {
size_t operator()(const DefaultEdge<Vertex> &edge) const {
  return edge.GetFrom() ^ edge.GetTo();
}
};
}

namespace graphs {

template<typename Vertex>
DefaultEdge<Vertex> GetReverseEdge(const DefaultEdge<Vertex> &edge) {
  return {edge.GetTo(), edge.GetFrom()};
}

template<typename VertexType = int, typename EdgeType = DefaultEdge<VertexType>>
class Graph {
 public:
  using Vertex = VertexType;
  using Edge = EdgeType;
  using InternalEdgeContainer = std::unordered_set<EdgeType>;

  explicit Graph(size_t number_vertices) : number_vertices_(number_vertices) {}

  void AddEdge(const EdgeType &edge) {
    adjacency_list_[edge.GetFrom()].insert({edge.GetFrom(), edge.GetTo()});
    ++number_edges_;
  }

  void AddVertex() {
    adjacency_list_.emplace_back();
    ++number_vertices_;
  }

  size_t VerticesCount() const {
    return number_vertices_;
  }

  size_t EdgesCount() const {
    return number_edges_;
  }

  // TODO (students): Fix const.
  InternalEdgeContainer &EdgesFromVertex(int vertex) {
    return adjacency_list_[vertex];
  }

  size_t EdgesNumberFromVertex(int vertex) const {
    return adjacency_list_[vertex].size();
  }

 private:
  size_t number_edges_ = 0;
  size_t number_vertices_ = 0;
  std::unordered_map<Vertex, std::unordered_set<Edge>> adjacency_list_;
};
}

#endif //TEACHING_SEMESTER_2_SEMINAR10_GRAPH_HPP_
