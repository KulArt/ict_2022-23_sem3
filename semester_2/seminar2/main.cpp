//
// Created by Артур Кулапин on 08.02.2023.
//

#include <iostream>

#include "adjacency_graph.hpp"
#include "bfs_ancestor_visitor.hpp"
#include "bfs.hpp"

int main() {
  std::vector<graph::DefaultEdge<int>> edges = {{1, 3}, {3, 2}, {2, 4}, {2, 1}, {2, 3}};
  graph::AdjacencyListGraph<int, graph::DefaultEdge<int>> graph(5, edges);

  traverses::visitors::AncestorBfsVisitor<int, graph::DefaultEdge<int>> visitor;
  traverses::BreadthFirstSearch(1, graph, visitor);
  auto map = visitor.GetMap();

}
