//
// Created by Артур Кулапин on 08.02.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR_2_ABSTRACT_GRAPH_HPP_
#define TEACHING_SEMESTER_2_SEMINAR_2_ABSTRACT_GRAPH_HPP_

#include <unordered_map>
#include <vector>
#include <utility>

namespace graph {
template <typename T>
struct DefaultEdge : std::pair<T, T> {
  DefaultEdge(const T& first, const T& second) : std::pair<T, T>(first, second) {}
  using BaseClass = std::pair<T, T>;
  const T& Start() const { return BaseClass::first; }
  const T& Finish() const { return BaseClass::second; }
};

template <typename Vertex = int, typename Edge = DefaultEdge<Vertex>>
class AbstractGraph {
 public:
  using VertexType = Vertex;
  using EdgeType = Edge;

  explicit AbstractGraph(size_t vertices_num, size_t edges_num = 0) :
      vertices_number_(vertices_num), edges_number_(edges_num) {}

  size_t GetVerticesNumber() const { return vertices_number_; }
  size_t GetEdgesNumber() const { return edges_number_; }

  virtual std::vector<Vertex> GetNeighbours(const Vertex& vertex) = 0;
 protected:
  size_t vertices_number_ = 0;
  size_t edges_number_ = 0;
};

}

#endif //TEACHING_SEMESTER_2_SEMINAR_2_ABSTRACT_GRAPH_HPP_
