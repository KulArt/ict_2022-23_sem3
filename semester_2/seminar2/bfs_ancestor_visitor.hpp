//
// Created by Артур Кулапин on 08.02.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR_2_BFS_ANCESTOR_VISITOR_HPP_
#define TEACHING_SEMESTER_2_SEMINAR_2_BFS_ANCESTOR_VISITOR_HPP_

#include "bfs_visitor.hpp"

#include "iostream"

namespace traverses::visitors {
template <class Vertex, class Edge>
class AncestorBfsVisitor : BfsVisitor<Vertex, Edge> {
 public:
  virtual void TreeEdge(const Edge& edge) {
    ancestors_[edge.Finish()] = edge.Start();
  }

  std::unordered_map<Vertex, Vertex> GetMap() const {
    return ancestors_;
  }

  virtual ~AncestorBfsVisitor() = default;

 private:
  std::unordered_map<Vertex, Vertex> ancestors_;
};
}

#endif //TEACHING_SEMESTER_2_SEMINAR_2_BFS_ANCESTOR_VISITOR_HPP_
