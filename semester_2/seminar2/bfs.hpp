//
// Created by Артур Кулапин on 08.02.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR_2_BFS_H_
#define TEACHING_SEMESTER_2_SEMINAR_2_BFS_H_

#include <queue>
#include <unordered_set>
#include <unordered_map>

namespace traverses {

template <class Vertex, class Graph, class Visitor>
void BreadthFirstSearch(Vertex origin_vertex, Graph &graph,
                        Visitor visitor) {
  std::queue<Vertex> bfs_queue;
  std::unordered_set<Vertex> visited_vertices;

  bfs_queue.push(origin_vertex);
  visited_vertices.insert(origin_vertex);

  while (!bfs_queue.empty()) {
    auto cur_vertex = bfs_queue.front();
    bfs_queue.pop();
    for (auto& neighbour : graph.GetNeighbours(cur_vertex)) {
      if (visited_vertices.find(neighbour) == visited_vertices.end()) {
        visitor.TreeEdge({cur_vertex, neighbour});
        bfs_queue.push(neighbour);
        visited_vertices.insert(neighbour);
      }
    }
  }
}
}

#endif //TEACHING_SEMESTER_2_SEMINAR_2_BFS_H_
