//
// Created by Артур Кулапин on 08.02.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR_2_ADJACENCY_GRAPH_HPP_
#define TEACHING_SEMESTER_2_SEMINAR_2_ADJACENCY_GRAPH_HPP_

#include "abstract_graph.hpp"

namespace graph {

template <typename Vertex = int, typename Edge = DefaultEdge<Vertex>>
class AdjacencyListGraph : public AbstractGraph<Vertex, Edge> {
 public:
  AdjacencyListGraph(size_t vertices_num, const std::vector<Edge>& edges) :
      AbstractGraph<Vertex, Edge>(vertices_num, edges.size()) {
    for (const auto& edge : edges) {
      list_[edge.Start()].push_back(edge.Finish());
      list_[edge.Finish()].push_back(edge.Start());
    }
  }

  std::vector<Vertex> GetNeighbours(const Vertex& vertex) final {
    return list_[vertex];
  }

 private:
  std::unordered_map<Vertex, std::vector<Vertex>> list_;
};
}

#endif //TEACHING_SEMESTER_2_SEMINAR_2_ADJACENCY_GRAPH_HPP_
