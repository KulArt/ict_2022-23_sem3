//
// Created by Артур Кулапин on 08.02.2023.
//

#ifndef TEACHING_SEMESTER_2_SEMINAR_2_BFS_VISITOR_HPP_
#define TEACHING_SEMESTER_2_SEMINAR_2_BFS_VISITOR_HPP_

namespace traverses::visitors {
template <class Vertex, class Edge>
class BfsVisitor {
 public:
  virtual void TreeEdge(const Edge& /*edge*/) = 0;
  virtual ~BfsVisitor() = default;
};
}

#endif //TEACHING_SEMESTER_2_SEMINAR_2_BFS_VISITOR_HPP_
