//
// Created by Артур Кулапин on 31.01.2023.
//

#ifndef TEACHING_SEMESTER_3_SEM1_SEQUENCE_HASHER_HPP_
#define TEACHING_SEMESTER_3_SEM1_SEQUENCE_HASHER_HPP_

#include <algorithm>
#include <exception>
#include <vector>

template <class Container>
class SequenceHasher {
 public:
  SequenceHasher() = default;
  explicit SequenceHasher(const Container* container) : container_(container) {
    CalculateBaseDegrees();
    BuildPrefixHashes();
  }

  // Calculates hash in [left, right).
  // For efficient computations requires random access category for Container::iterator.
  size_t CalculateSubsegmentHash(typename Container::const_iterator left,
                                 typename Container::const_iterator right) const {
    const auto left_idx = std::distance(container_->begin(), left);
    const auto right_idx = std::distance(container_->begin(), right);

    return CalculateSubsegmentHash(left_idx, right_idx);
  }

  // Calculates hash in [left, right).
  size_t CalculateSubsegmentHash(size_t left, size_t right) const {
    if (!CheckIndicesValidness(left, right)) {
      throw std::logic_error("Indices are invalid!!");
    }
    const uint64_t coeff = base_degrees_[right - left];
    return (prefix_hashes_[right] - (prefix_hashes_[left] * coeff)) % kModulo;
  }

 private:
  bool CheckIndicesValidness(size_t left, size_t right) const {
    return !(left > right || left > container_->size() || right > container_->size());
  }

  void BuildPrefixHashes();
  void CalculateBaseDegrees();

  // Must be prime.
  static constexpr uint64_t kModulo = 1e9 + 7;
  // Must be less than kModulo.
  static constexpr uint64_t kBase = 2;
  const Container* container_;
  std::vector<uint64_t> prefix_hashes_;
  std::vector<uint64_t> base_degrees_;
};

template <class Container>
void SequenceHasher<Container>::CalculateBaseDegrees() {
  base_degrees_.resize(container_->size(), 1);
  for (size_t i = 1; i < container_->size(); ++i) {
    base_degrees_[i] = (base_degrees_[i - 1] * kBase) % kModulo;
  }
}

template <class Container>
void SequenceHasher<Container>::BuildPrefixHashes() {
  prefix_hashes_.resize(container_->size() + 1, 0);
  size_t cur_idx = 1;
  for (auto& elem : *container_) {
    const uint64_t cur_elem_hash = std::hash<typename Container::value_type>()(elem) % kModulo;
    prefix_hashes_[cur_idx] = (prefix_hashes_[cur_idx - 1] * kBase + cur_elem_hash) % kModulo;
    ++cur_idx;
  }
}

#endif //TEACHING_SEMESTER_3_SEM1_SEQUENCE_HASHER_HPP_