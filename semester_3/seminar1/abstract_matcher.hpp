//
// Created by Артур Кулапин on 31.01.2023.
//

#pragma once

#include <algorithm>

template <class Container, class Callback>
class AbstractMatcher {
 public:
  explicit AbstractMatcher(Container text, Callback callback) : text_(std::move(text)), callback_(callback) {}
  virtual void Match(const Container& pattern) const = 0;

 protected:
  Callback callback_;
  Container text_;
};
