#include "sequence_hasher.hpp"

#include <iostream>
#include <string>

int main() {
  std::string text = "abacaba";
  std::string pattern = "aba";
  SequenceHasher<std::string> pattern_hasher(&pattern);
  SequenceHasher<std::string> text_hasher(&text);
  for (int i = 0; i < text.size() - pattern.size() + 1; ++i) {
    std::cout << i << ' ' << i + 2 << ' '
              << text_hasher.CalculateSubsegmentHash(i, i + 3) << ' '
              << pattern_hasher.CalculateSubsegmentHash(0, 3) << '\n';
  }
}
