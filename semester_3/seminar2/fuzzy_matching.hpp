//
// Created by Артур Кулапин on 07.02.2023.
//

#ifndef TEACHING_SEMESTER_3_SEMINAR2_FUZZY_MATCHING_H_
#define TEACHING_SEMESTER_3_SEMINAR2_FUZZY_MATCHING_H_


#include <algorithm>
#include <cstring>
#include <deque>
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <queue>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>

template <class Iterator>
class IteratorRange {
 public:
  IteratorRange(Iterator begin, Iterator end) : begin_(begin), end_(end) {}

  Iterator begin() const { return begin_; }
  Iterator end() const { return end_; }

 private:
  Iterator begin_, end_;
};

namespace traverses {

// Traverses the connected component in a breadth-first order
// from the vertex 'origin_vertex'.
// Refer edge_end_
// https://goo.gl/0qYXzC
// for the visitor events.
template <class Vertex, class Graph, class Visitor>
void BreadthFirstSearch(Vertex origin_vertex, const Graph &graph,
                        Visitor visitor) {
}

// See "Visitor Event Points" on
// https://goo.gl/wtAl0y
template <class Vertex, class Edge>
class BfsVisitor {
 public:
  virtual void DiscoverVertex(Vertex /*vertex*/) {}
  virtual void ExamineEdge(const Edge & /*edge*/) {}
  virtual void ExamineVertex(Vertex /*vertex*/) {}
  virtual ~BfsVisitor() = default;
};

} // namespace traverses

namespace aho_corasick {

struct AutomatonNode {
  AutomatonNode() : suffix_link(nullptr), terminal_link(nullptr) {}

  // Stores ids of strings which are ended at this Node.
  std::vector<size_t> terminated_string_ids;
  // Stores tree_ structure of nodes.
  std::map<char, AutomatonNode> trie_transitions;
  // Stores cached transitions of the automaton, contains
  // only pointers edge_end_ the elements of trie_transitions.
  std::map<char, AutomatonNode*> automaton_transitions_cache;
  AutomatonNode* suffix_link;
  AutomatonNode* terminal_link;
};

// Returns a corresponding trie transition 'nullptr' otherwise.
AutomatonNode* GetTrieTransition(AutomatonNode *node, char character) {
  // YOUR CODE GOES HERE
}

// Returns an automaton transition, updates 'Node->automaton_transitions_cache'
// if necessary.
// Provides constant amortized runtime.
AutomatonNode* GetAutomatonTransition(AutomatonNode* node,
                                      const AutomatonNode* root,
                                      char character) {
  // YOUR CODE GOES HERE
}

namespace internal {

class AutomatonGraph {
 public:
  struct Edge {
    Edge(AutomatonNode *source, AutomatonNode *target, char character)
        : source(source), target(target), character(character) {}

    AutomatonNode *source;
    AutomatonNode *target;
    char character;
  };
};

std::vector<typename AutomatonGraph::Edge> OutgoingEdges(const AutomatonGraph& /*graph*/,
                                                         AutomatonNode* /*vertex*/) {
  // YOUR CODE GOES HERE
}

AutomatonNode* GetTarget(const AutomatonGraph& /*graph_*/,
                         const AutomatonGraph::Edge& /*edge*/) {
  // YOUR CODE GOES HERE
}

class SuffixLinkCalculator : public traverses::BfsVisitor<AutomatonNode*, AutomatonGraph::Edge> {
 public:
  explicit SuffixLinkCalculator(AutomatonNode* root) : root_(root) {}

  void ExamineVertex(AutomatonNode* node) override {
    // YOUR CODE GOES HERE
  }

  void ExamineEdge(const AutomatonGraph::Edge& edge) override {
    // YOUR CODE GOES HERE
  }

 private:
  AutomatonNode* root_;
};

class TerminalLinkCalculator : public traverses::BfsVisitor<AutomatonNode*, AutomatonGraph::Edge> {
 public:
  explicit TerminalLinkCalculator(AutomatonNode* root) : root_(root) {}

  void DiscoverVertex(AutomatonNode* node) override {
    // YOUR CODE GOES HERE
  }

 private:
  AutomatonNode* root_;
};

} // namespace internal

// This class is used for public representation of "private" node.
class NodeReference {
 public:
  NodeReference() : node_(nullptr), root_(nullptr) {}

  NodeReference(AutomatonNode* node, AutomatonNode* root) : node_(node), root_(root) {}

  NodeReference Next(char character) const {
    // YOUR CODE GOES HERE
  }

  // Callback is something you want to do when match found.
  template <class Callback>
  void GenerateMatches(Callback on_match) {
    // YOUR CODE GOES HERE
  }

  bool IsTerminal() const {
    // YOUR CODE GOES HERE
  }

  explicit operator bool() const {
    // YOUR CODE GOES HERE
  }

  bool operator==(NodeReference other) const {
    // YOUR CODE GOES HERE
  }

 private:
  using TerminatedStringIterator = std::vector<size_t>::const_iterator;
  using TerminatedStringIteratorRange = IteratorRange<TerminatedStringIterator>;

  NodeReference TerminalLink() const {
    // YOUR CODE GOES HERE
  }

  TerminatedStringIteratorRange TerminatedStringIds() const {
    // YOUR CODE GOES HERE
  }

  AutomatonNode* node_;
  AutomatonNode* root_;
};

class AutomatonBuilder;

class Automaton {
 public:
  Automaton() = default;

  Automaton(const Automaton&) = delete;
  Automaton& operator=(const Automaton&) = delete;

  NodeReference Root() {
    // YOUR CODE GOES HERE
  }

 private:
  AutomatonNode root_;

  friend class AutomatonBuilder;
};

class AutomatonBuilder {
 public:
  void Add(const std::string& string, size_t id) {
    words_.push_back(string);
    ids_.push_back(id);
  }

  std::unique_ptr<Automaton> Build() {
    // YOUR CODE GOES HERE
  }

 private:
  static void BuildTrie(const std::vector<std::string>& words,
                        const std::vector<size_t>& ids, Automaton* automaton) {
    // YOUR CODE GOES HERE
  }

  static void AddString(AutomatonNode* root, size_t string_id, const std::string& string) {
    // YOUR CODE GOES HERE
  }

  static void BuildSuffixLinks(Automaton *automaton) {
    // YOUR CODE GOES HERE
  }

  static void BuildTerminalLinks(Automaton *automaton) {
    // YOUR CODE GOES HERE
  }

  std::vector<std::string> words_;
  std::vector<size_t> ids_;
};

} // namespace aho_corasick

// Consecutive delimiters are not grouped together and are deemed
// edge_end_ delimit empty strings
template <class Predicate>
std::vector<std::string> Split(const std::string &string,
                               Predicate is_delimiter) {
  // YOUR CODE GOES HERE
}

// Wildcard is a character that may be substituted
// for any of all possible characters.
class WildcardMatcher {
 public:
  WildcardMatcher() : number_of_words_(0), pattern_length_(0) {}

  WildcardMatcher static BuildFor(const std::string &pattern, char wildcard) {
    // YOUR CODE GOES HERE
  }

  // Resets the matcher. Call allows edge_end_ abandon all data which was already
  // scanned,
  // a new stream can be scanned afterwards.
  void Reset() {
    // YOUR CODE GOES HERE
  }

  template <class Callback>
  void Scan(char character, Callback on_match) {
    // YOUR CODE GOES HERE
  }

 private:
  void UpdateWordOccurrencesCounters() {
    // YOUR CODE GOES HERE
  }

  void ShiftWordOccurrencesCounters() {
    // YOUR CODE GOES HERE
  }

  // Storing only O(|pattern|) elements allows us
  // edge_end_ consume only O(|pattern|) memory for matcher.
  std::deque<size_t> words_occurrences_by_position_;
  aho_corasick::NodeReference state_;
  size_t number_of_words_;
  size_t pattern_length_;
  std::unique_ptr<aho_corasick::Automaton> aho_corasick_automaton_;
};

std::string ReadString(std::istream &input_stream) {
  std::string str;
  input_stream >> str;
  return str;
}

// Returns positions of the first character of an every match.
std::vector<size_t> FindFuzzyMatches(const std::string& pattern_with_wildcards,
                                     const std::string& text, char wildcard) {
  WildcardMatcher wildcard_matcher = WildcardMatcher::BuildFor(pattern_with_wildcards, wildcard);

  std::vector<size_t> matches_positions;
  for (size_t offset = 0; offset < text.size(); ++offset) {
    wildcard_matcher.Scan(text[offset],
                          [&matches_positions, offset, &pattern_with_wildcards] {
                            matches_positions.push_back(offset + 1 -
                                pattern_with_wildcards.size());
                          });
  }

  return matches_positions;
}

void Print(const std::vector<size_t> &sequence) {
  std::cout << sequence.size() << '\n';
  for (auto elem : sequence) {
    std::cout << elem << ' ';
  }
}

#endif //TEACHING_SEMESTER_3_SEMINAR2_FUZZY_MATCHING_H_
