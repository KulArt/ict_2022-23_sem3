# Семинар 2. Бор. Алгоритм Ахо-Корасик.

## Теоретические задачи

1. В множестве `S` лежат `n` чисел: $`x_1,\ldots , x_n`$. Для каждого из чисел $`y_1, \ldots, y_q`$ найдите $`\max(y_i \oplus x_j)`$. 
Проделайте то же для минимума. В предположении, что все числа целые и лежат в отрезке $`[0, 2^k - 1]`$, отвечайте на каждый запрос за `O(k)`.

<details>
<summary>Решение.</summary>

Будем считать числа битовыми строками. Для максимума построим бор на этих битовых строках. Далее будем для каждого `y` жадно спускаться.
Тогда, в силу того, что результат на верхнем уровне заведомо превзойдет суммарный результат на всех нижних, это корректно.

</details>

2. К множеству `S` поступают запросы двух видов: добавить строку в `S`; сообщить `k`-ю строку `S` в лексикографическом порядке 
(`k` - параметр запроса). Отвечайте на запрос за линейное время от длины строки.

<details>
<summary>Решение.</summary>

Храните в поддереве число терминалов, тогда поиск `k`-й порядковой строки в боре похож на поиск `k`-й порядковой статистики в дереве поиска.

</details>

3. Дана строка `S` над алфавитом $`\Sigma`$. Найдите такую строку `T`, что ее длина минимальна, при этом она не является подстрокой `S`. 
Среди всех таких найдите лексикографически минимальную.

<details>
<summary>Решение.</summary>

Определим $`l = \lceil \log_{|\Sigma|}|S|^2 \rceil`$, это ограничение сверху на длину ответа. 
Почему это так? Число различных подстрок в `S` не превосходит $`|S|^2`$, тогда среди $`|\Sigma|^l`$ строк найдется та, которой нет в `S`.

Теперь добавим все подстроки `S` длины `l` в бор. Найдите для каждой вершины минимальную длину нисходящего пути до вершины, чья степень строго меньше $`|\Sigma|`$ динамикой по поддеревьям. 
Тогда вы узнаете истинную длину ответа. Далее с помощью DFS найдите ответ.
    
Итоговые время и память: $`O(|S|\log|S|)`$.

</details>

4. Предложите структуру данных, позволяющую хранить набор строк и выполнять следующие запросы:
* Добавление строки `S` в набор за учетные $`O(|S|\log N)`$, где `N` - текущий размер набора.
* Для текста `T` найти суммарное вхождение всех строк из набора паттернов в текст за $`O(|T|\log N)`$

<details>
<summary>Решение.</summary>

Будем хранить множество автоматов Ахо-Корасик, в каждом будет $`2^k`$ паттернов, где `k` попарно различны.
Тогда добавление строки эквивалентно
* если нет автомата для `k = 0`, то построить для единственной строки автомат префикс-функции;
* если есть автоматы для всех `i < k`, то соберем все строки из данных автоматах и построим на множестве размера $`2^k`$ новый автомат.

Тогда каждая строка поучаствует суммарно не более чем в $`\log N`$ автоматах, то есть перестраивать автомат с ней придется редко.

Поиск же строк - поиск по всем автоматам.

</details>

5. Дана строка `S` с не более чем `k` знаками вопроса. Вхождением `S` в текст `T` назовём подстроку `T`, которая совпадает с `S`
во всех символах, кроме вопросов. Найдите все вхождения `S` в `T`. Используйте `O(|S|)` памяти.

<details>
<summary>Решение.</summary>

Разбейте строку по знакам вопроса, затем постройте на них автомат Ахо-Корасик. 
Далее идите строкой по автомату и отмечайте вхождение `i`-го паттерна как вхождение паттерна в позиции `i` минус длина префикса до `i`. 
Все позиции `i` такие, что в них число вхождений равно числу паттернов дадут полное совпадение.

Для использования `O(|S|)` памяти стоит отметить, что данный алгоритм работает в режиме онлайн. 
Поэтому достаточно хранить только `O(|S|)` от строки `T`, а вместо массива встреч хранить дек. 

</details>

6. Рассмотрим алфавит $`|\Sigma|`$ и `n` строк в нём: $`S_1, \ldots , S_n`$, причём `|S_i| < m` для всех `i`. 
Слово `t` назовём хорошим, если в нём можно выделить несколько подстрок, каждая из которых равна какому-нибудь $`S_i`$, 
и все символы при этом находятся хотя бы в одной из выделенных подстрок (то есть `t` покрыта словарным словами). 
Найдите число хороших строк длины `k` за полиномиальное время.

<details>
<summary>Решение.</summary>

Построим автомат Ахо-Корасик по строкам $`S_i`$. Далее введем динамику $`dp[v][l][f]`$ - число таких строк длины `l`, 
что их чтение закончилось в вершине `v`, а непокрытый суффикс имеет длину $f$. 
Пересчитывайте динамику с помощью BFS по автомату, при этом `f` может либо увеличиться на 1, либо же занулиться, если вы пришли в терминал.
Ответ: сумма по всем вершинам `v` `dp[v][k][0]`.

Сколько различных состояний может быть? Таких будет для каждой вершины $v$ порядка `O(mk)`, 
так как длина непокрытого суффикса не может быть больше $m$ (иначе вы никак не покроете строку полностью). 
Число вершин будет порядка `O(nm)`, так как бор может быть устроен как $n$ веток длины $m$. 
Вспомним еще, что пересчет динамики требует прохода по всему алфавиту из каждого состояния, откуда итоговое время: $`O(knm^2|\Sigma|)`$.

</details>

