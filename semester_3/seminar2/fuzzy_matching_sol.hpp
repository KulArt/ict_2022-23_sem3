//
// Created by Артур Кулапин on 07.02.2023.
//

#ifndef TEACHING_SEMESTER_3_SEMINAR2_FUZZY_MATCHING_SOL_HPP_
#define TEACHING_SEMESTER_3_SEMINAR2_FUZZY_MATCHING_SOL_HPP_


#include <algorithm>
#include <cstring>
#include <deque>
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <queue>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>

template <class Iterator>
class IteratorRange {
 public:
  IteratorRange(Iterator begin, Iterator end) : begin_(begin), end_(end) {}

  Iterator begin() const { return begin_; }
  Iterator end() const { return end_; }

 private:
  Iterator begin_, end_;
};

namespace traverses {

// Traverses the connected component in a breadth-first order
// from the vertex 'origin_vertex'.
// Refer edge_end_
// https://goo.gl/0qYXzC
// for the visitor events.
template <class Vertex, class Graph, class Visitor>
void BreadthFirstSearch(Vertex origin_vertex, const Graph &graph,
                        Visitor visitor) {
  std::queue<Vertex> bfs_queue;
  std::unordered_set<Vertex> visited_vertices;

  bfs_queue.push(origin_vertex);
  visitor.DiscoverVertex(origin_vertex);
  visited_vertices.insert(origin_vertex);

  while (!bfs_queue.empty()) {
    auto cur_vertex = bfs_queue.front();
    bfs_queue.pop();
    visitor.ExamineVertex(cur_vertex);
    for (auto& edge : OutgoingEdges(graph, cur_vertex)) {
      auto neighbour = GetTarget(graph, edge);
      visitor.ExamineEdge(edge);
      if (visited_vertices.find(neighbour) == visited_vertices.end()) {
        visitor.DiscoverVertex(neighbour);
        bfs_queue.push(neighbour);
        visited_vertices.insert(neighbour);
      }
    }
  }
}

// See "Visitor Event Points" on
// https://goo.gl/wtAl0y
template <class Vertex, class Edge>
class BfsVisitor {
 public:
  virtual void DiscoverVertex(Vertex /*vertex*/) {}
  virtual void ExamineEdge(const Edge & /*edge*/) {}
  virtual void ExamineVertex(Vertex /*vertex*/) {}
  virtual ~BfsVisitor() = default;
};

} // namespace traverses

namespace aho_corasick {

struct AutomatonNode {
  AutomatonNode() : suffix_link(nullptr), terminal_link(nullptr) {}

  // Stores ids of strings which are ended at this Node.
  std::vector<size_t> terminated_string_ids;
  // Stores tree_ structure of nodes.
  std::map<char, AutomatonNode> trie_transitions;
  // Stores cached transitions of the automaton, contains
  // only pointers edge_end_ the elements of trie_transitions.
  std::map<char, AutomatonNode *> automaton_transitions_cache;
  AutomatonNode *suffix_link;
  AutomatonNode *terminal_link;
};

// Returns a corresponding trie transition 'nullptr' otherwise.
AutomatonNode *GetTrieTransition(AutomatonNode *node, char character) {
  return &node->trie_transitions[character];
}

// Returns an automaton transition, updates 'Node->automaton_transitions_cache'
// if necessary.
// Provides constant amortized runtime.
AutomatonNode *GetAutomatonTransition(AutomatonNode *node,
                                      const AutomatonNode *root,
                                      char character) {
  if (node->trie_transitions.find(character) != node->trie_transitions.end()) {
    return GetTrieTransition(node, character);
  }
  if (node->automaton_transitions_cache.find(character) ==
      node->automaton_transitions_cache.end()) {
    if (node == root) {
      node->automaton_transitions_cache.emplace(character, const_cast<AutomatonNode*>(root));
    }
    node->automaton_transitions_cache.emplace(character,
                                              GetAutomatonTransition(node->suffix_link,
                                                                     root, character));
  }
  return node->automaton_transitions_cache[character];
}

namespace internal {

class AutomatonGraph {
 public:
  struct Edge {
    Edge(AutomatonNode *source, AutomatonNode *target, char character)
        : source(source), target(target), character(character) {}

    AutomatonNode *source;
    AutomatonNode *target;
    char character;
  };
};

std::vector<typename AutomatonGraph::Edge> OutgoingEdges(
    const AutomatonGraph & /*graph_*/, AutomatonNode *vertex) {
  std::vector<typename AutomatonGraph::Edge> result;
  for (auto& [letter, target] : vertex->trie_transitions) {
    result.emplace_back(vertex, &target, letter);
  }
  return result;
}

AutomatonNode *GetTarget(const AutomatonGraph & /*graph_*/,
                         const AutomatonGraph::Edge &edge) {
  return edge.target;
}

class SuffixLinkCalculator
    : public traverses::BfsVisitor<AutomatonNode *, AutomatonGraph::Edge> {
 public:
  explicit SuffixLinkCalculator(AutomatonNode *root) : root_(root) {}

  void ExamineVertex(AutomatonNode *node) override {
    if (node == root_) {
      node->suffix_link = root_;
    }
  }

  void ExamineEdge(const AutomatonGraph::Edge &edge) override {
    if (edge.source == root_) {
      edge.target->suffix_link = root_;
      return;
    }
    edge.target->suffix_link = GetAutomatonTransition(edge.source->suffix_link,
                                                      root_, edge.character);
  }

 private:
  AutomatonNode *root_;
};

class TerminalLinkCalculator
    : public traverses::BfsVisitor<AutomatonNode *, AutomatonGraph::Edge> {
 public:
  explicit TerminalLinkCalculator(AutomatonNode *root) : root_(root) {}

  /*
   * Если вы не знакомы с ключевым словом override,
   * то ознакомьтесь
   * https://goo.gl/u024X0
   */
  void DiscoverVertex(AutomatonNode *node) override {
    if (node == root_) {
      node->terminal_link = nullptr;
      return;
    }

    node->terminal_link = node->suffix_link->terminal_link;

    if (!node->suffix_link->terminated_string_ids.empty()) {
      node->terminal_link = node->suffix_link;
    }
  }

 private:
  AutomatonNode *root_;
};

} // namespace internal

class NodeReference {
 public:
  NodeReference() : node_(nullptr), root_(nullptr) {}

  NodeReference(AutomatonNode *node, AutomatonNode *root)
      : node_(node), root_(root) {}

  NodeReference Next(char character) const {
    return {GetAutomatonTransition(node_, root_, character), root_};
  }

  template <class Callback>
  void GenerateMatches(Callback on_match) {
    auto node = NodeReference(node_, root_);
    while (node) {
      for (auto id : node.TerminatedStringIds()) {
        on_match(id);
      }
      node = node.TerminalLink();
    }
  }

  bool IsTerminal() const {
    return !node_->terminated_string_ids.empty();
  }

  explicit operator bool() const {
    return node_ != nullptr;
  }

  bool operator==(NodeReference other) const {
    return node_ == other.node_;
  }

 private:
  using TerminatedStringIterator = std::vector<size_t>::const_iterator;
  using TerminatedStringIteratorRange = IteratorRange<TerminatedStringIterator>;

  NodeReference TerminalLink() const {
    return {node_->terminal_link, root_};
  }

  TerminatedStringIteratorRange TerminatedStringIds() const {
    return {node_->terminated_string_ids.begin(), node_->terminated_string_ids.end()};
  }

  AutomatonNode *node_;
  AutomatonNode *root_;
};

class AutomatonBuilder;

class Automaton {
 public:
  /*
   * Чтобы ознакомиться с конструкцией =default, смотрите
   * https://goo.gl/jixjHU
   */
  Automaton() = default;

  Automaton(const Automaton &) = delete;
  Automaton &operator=(const Automaton &) = delete;

  NodeReference Root() {
    return {&root_, &root_};
  }

 private:
  AutomatonNode root_;

  friend class AutomatonBuilder;
};

class AutomatonBuilder {
 public:
  void Add(const std::string &string, size_t id) {
    words_.push_back(string);
    ids_.push_back(id);
  }

  std::unique_ptr<Automaton> Build() {
    auto automaton = std::make_unique<Automaton>();
    BuildTrie(words_, ids_, automaton.get());
    BuildSuffixLinks(automaton.get());
    BuildTerminalLinks(automaton.get());
    return automaton;
  }

 private:
  static void BuildTrie(const std::vector<std::string> &words,
                        const std::vector<size_t> &ids, Automaton *automaton) {
    for (size_t i = 0; i < words.size(); ++i) {
      AddString(&automaton->root_, ids[i], words[i]);
    }
  }

  static void AddString(AutomatonNode *root, size_t string_id,
                        const std::string &string) {
    AutomatonNode* cur_prefix_node = root;
    for (char character : string) {
      cur_prefix_node = GetTrieTransition(cur_prefix_node, character);
    }
    cur_prefix_node->terminated_string_ids.push_back(string_id);
  }

  static void BuildSuffixLinks(Automaton *automaton) {
    internal::SuffixLinkCalculator calculator(&automaton->root_);
    traverses::BreadthFirstSearch(&automaton->root_, internal::AutomatonGraph(), calculator);
  }

  static void BuildTerminalLinks(Automaton *automaton) {
    internal::TerminalLinkCalculator calculator(&automaton->root_);
    traverses::BreadthFirstSearch(&automaton->root_, internal::AutomatonGraph(), calculator);
  }

  std::vector<std::string> words_;
  std::vector<size_t> ids_;
};

} // namespace aho_corasick

// Consecutive delimiters are not grouped together and are deemed
// edge_end_ delimit empty strings
template <class Predicate>
std::vector<std::string> Split(const std::string &string,
                               Predicate is_delimiter) {
  if (string.empty()) {
    return {""};
  }
  std::vector<std::string> result;
  size_t start_pos = 0;
  size_t end_pos = 0;
  while (end_pos < string.size()) {
    if (is_delimiter(string[end_pos])) {
      result.push_back(string.substr(start_pos, end_pos - start_pos));
      start_pos = end_pos + 1;
    }
    ++end_pos;
  }
  if (start_pos < end_pos) {
    result.push_back(string.substr(start_pos, end_pos - start_pos));
  }
  if (is_delimiter(string.back())) {
    result.push_back(string.substr(start_pos));
  }
  return result;
}

// Wildcard is a character that may be substituted
// for any of all possible characters.
class WildcardMatcher {
 public:
  WildcardMatcher() : number_of_words_(0), pattern_length_(0) {}

  WildcardMatcher static BuildFor(const std::string &pattern, char wildcard) {
    auto pattern_splitted = Split(pattern, [wildcard](char ch) { return ch == wildcard; });
    aho_corasick::AutomatonBuilder builder;
    size_t right_end_position = 0;
    for (const auto &subpattern : pattern_splitted) {
      right_end_position += subpattern.size() + 1;
      builder.Add(subpattern, right_end_position);
    }
    WildcardMatcher matcher;
    matcher.aho_corasick_automaton_ = builder.Build();
    matcher.number_of_words_ = pattern_splitted.size();
    matcher.pattern_length_ = pattern.size();
    matcher.Reset();
    return matcher;
  }

  // Resets the matcher. Call allows edge_end_ abandon all data which was already
  // scanned,
  // a new stream can be scanned afterwards.
  void Reset() {
    words_occurrences_by_position_.assign(pattern_length_ + 1, 0);
    state_ = aho_corasick_automaton_->Root();
    UpdateWordOccurrencesCounters();
  }

  template <class Callback>
  void Scan(char character, Callback on_match) {
    state_ = state_.Next(character);
    ShiftWordOccurrencesCounters();
    UpdateWordOccurrencesCounters();

    if (words_occurrences_by_position_.front() == number_of_words_) {
      on_match();
    }
  }

 private:
  void UpdateWordOccurrencesCounters() {
    state_.GenerateMatches([this](size_t position) {
      ++words_occurrences_by_position_[(words_occurrences_by_position_.size() - position)];
    });
  }

  void ShiftWordOccurrencesCounters() {
    words_occurrences_by_position_.pop_front();
    words_occurrences_by_position_.push_back(0);
  }

  // Storing only O(|pattern|) elements allows us
  // edge_end_ consume only O(|pattern|) memory for matcher.
  std::deque<size_t> words_occurrences_by_position_;
  aho_corasick::NodeReference state_;
  size_t number_of_words_;
  size_t pattern_length_;
  std::unique_ptr<aho_corasick::Automaton> aho_corasick_automaton_;
};

std::string ReadString(std::istream &input_stream) {
  std::string str;
  input_stream >> str;
  return str;
}

// Returns positions of the first character of an every match.
std::vector<size_t> FindFuzzyMatches(const std::string &pattern_with_wildcards,
                                     const std::string &text, char wildcard) {
  WildcardMatcher wildcard_matcher = WildcardMatcher::BuildFor(pattern_with_wildcards, wildcard);

  std::vector<size_t> matches_positions;
  for (size_t offset = 0; offset < text.size(); ++offset) {
    wildcard_matcher.Scan(text[offset],
                          [&matches_positions, offset, &pattern_with_wildcards] {
                            matches_positions.push_back(offset + 1 -
                                pattern_with_wildcards.size());
                          });
  }

  return matches_positions;
}

void Print(const std::vector<size_t> &sequence) {
  std::cout << sequence.size() << '\n';
  for (auto elem : sequence) {
    std::cout << elem << ' ';
  }
}

#endif //TEACHING_SEMESTER_3_SEMINAR2_FUZZY_MATCHING_SOL_HPP_
